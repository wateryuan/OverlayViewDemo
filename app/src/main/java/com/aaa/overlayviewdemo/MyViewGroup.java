package com.aaa.overlayviewdemo;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

/**
 * create by water.yuan
 * on 2020-09-27
 */
public class MyViewGroup extends LinearLayout {
    private static final String TAG = "MyViewGroup";

    private final WindowManager mWindowManager;
    private final int mStatusBarHeight;

    private View mMiniView; // 最小化的空白View
    private View mContentView; // 自定义的子View

    private OnClickListener mListener;

    private float mX;
    private float mY;
    private float mRawX;
    private float mRawY;
    private WindowManager.LayoutParams mLayoutParams;

    public MyViewGroup(@NonNull Context context) {
        super(context);

        mWindowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);

        mStatusBarHeight = AUtils.getStatusBarHeight(getContext());

        // 直接把xml添加到MyViewGroup里
        inflate(getContext(), R.layout.overlay_view, this);
        mMiniView = findViewById(R.id.mini_view);
        mContentView = findViewById(R.id.content_view);

        findViewById(R.id.btn_close).setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onClick(view);
            }
        });
    }

    /**
     * 实现手指拖动效果
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mX = event.getX();
                mY = event.getY();
                Log.i(TAG, "onTouchEvent:ACTION_DOWN mX=" + mX + " mY=" + mY + " mStatusHeight=" + mStatusBarHeight);
                break;
            case MotionEvent.ACTION_MOVE:
                mRawX = event.getRawX();
                mRawY = event.getRawY();
                mLayoutParams = (WindowManager.LayoutParams) getLayoutParams();
                // 过程点相对屏幕左边的坐标-落点相对MyViewGroup左边的坐标=MyViewGroup相对屏幕左边的坐标（布局参数的坐标）
                mLayoutParams.x = (int) (mRawX - mX);
                /*
                 * TODO: 2020/9/29
                 * 假设：状态栏高度100，mY为10，mRawY为200，则y实际为200-10-100=90，因为y从状态栏下边开始算
                 */
                mLayoutParams.y = (int) (mRawY - mY - mStatusBarHeight);
                Log.i(TAG, "onTouchEvent: mRawX=" + mRawX + " mRawY=" + mRawY);
                updateLayout(mLayoutParams);
                break;
            case MotionEvent.ACTION_UP:
                showTinyOrNot();
                break;
        }
        return true;
    }

    /**
     * 更新布局参数
     */
    private void updateLayout(WindowManager.LayoutParams params) {
        mWindowManager.updateViewLayout(this, params);
    }

    /**
     * 根据离屏幕左边的距离来
     * 判断显示最小化的空白View还是显示自定义的子View
     */
    private void showTinyOrNot() {
        mLayoutParams = (WindowManager.LayoutParams) getLayoutParams();
        if (mLayoutParams.x < 5) {
            mMiniView.setVisibility(VISIBLE);
            mContentView.setVisibility(GONE);
            mLayoutParams.x = 0;
            updateLayout(mLayoutParams);
        } else {
            mMiniView.setVisibility(GONE);
            mContentView.setVisibility(VISIBLE);
        }
    }

    public MyViewGroup setCallback(OnClickListener listener) {
        mListener = listener;
        return this;
    }
}
