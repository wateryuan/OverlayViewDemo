package com.aaa.overlayviewdemo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

/**
 * Request overlay window permission
 */
public class RequestOverlayActivity extends Activity {
    private static final int Request_Code_Overlay = 110;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, Request_Code_Overlay);
    }

    /**
     * 避免再次请求而死循环
     */
    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Request_Code_Overlay && Settings.canDrawOverlays(this)) {
            MyViewManager.showOverlayView(getApplication());
        } else {
            Toast.makeText(getApplicationContext(), "Window permission denied, cannot show activity task", Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}
